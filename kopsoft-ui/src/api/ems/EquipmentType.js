import request from '@/utils/request'

// 查询设备类型列表
export function listEquipmentType(query) {
  return request({
    url: '/ems/EquipmentType/list',
    method: 'get',
    params: query
  })
}

// 查询设备类型详细
export function getEquipmentType(typeId) {
  return request({
    url: '/ems/EquipmentType/' + typeId,
    method: 'get'
  })
}

// 新增设备类型
export function addEquipmentType(data) {
  return request({
    url: '/ems/EquipmentType',
    method: 'post',
    data: data
  })
}

// 修改设备类型
export function updateEquipmentType(data) {
  return request({
    url: '/ems/EquipmentType',
    method: 'put',
    data: data
  })
}

// 删除设备类型
export function delEquipmentType(typeId) {
  return request({
    url: '/ems/EquipmentType/' + typeId,
    method: 'delete'
  })
}
